import {create} from "../../../src/commands/create";
import {expect} from "chai";
import {move} from "../../../src/commands/move";

describe('move.ts', () => {
    it('move', () => {
        const root = {};
        create(root, 'fruits');
        create(root, 'vegetables');
        create(root, 'grains');
        create(root, 'fruits/apples');
        create(root, 'fruits/apples/fuji');
        create(root, 'grains/squash');
        move(root, 'grains/squash', 'vegetables')
        create(root, 'foods');
        move(root, 'grains', 'foods');
        move(root, 'fruits', 'foods');
        move(root, 'vegetables', 'foods');
        expect(root).to.have.nested.property('foods.grains');
        expect(root).to.have.nested.property('foods.fruits');
        expect(root).to.have.nested.property('foods.fruits.apples');
        expect(root).to.have.nested.property('foods.fruits.apples.fuji');
        expect(root).to.have.nested.property('foods.vegetables.squash');
    });
});