import { create } from '../../../src/commands/create'
import { list} from "../../../src/commands/list";
import { move} from "../../../src/commands/move";
import {remove} from "../../../src/commands/delete";

describe('list.js', () => {
    it('list', () => {
        const root = {};
        create(root, 'fruits');
        create(root, 'vegetables');
        create(root, 'grains');
        create(root, 'fruits/apples');
        create(root, 'fruits/apples/fuji');
        list(root)
        create(root, 'grains/squash');
        move(root, 'grains/squash', 'vegetables')
        create(root, 'foods');
        move(root, 'grains', 'foods');
        move(root, 'fruits', 'foods');
        move(root, 'vegetables', 'foods');
        list(root);
        remove(root, 'fruit/apples')
        remove(root, 'foods/fruits/apples')
        list(root)
    });
});