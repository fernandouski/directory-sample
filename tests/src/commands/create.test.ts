import { expect } from 'chai';
import { create } from '../../../src/commands/create'

describe('create.js', () => {
    it('create single fruit folder', () => {
        const root = {};
        create(root, 'fruits');
        expect(root).to.have.property('fruits');
    });
    it('create vegetables folder', () => {
        const root = {};
        create(root, 'vegetables');
        expect(root).to.have.property('vegetables');
    });
    it('create vegetables folder', () => {
        const root = {};
        create(root, 'grains');
        expect(root).to.have.property('grains');
    });
    it('create fruits/apples folder', () => {
        const root = {};
        create(root, 'fruits/apples');
        expect(root).to.have.nested.property('fruits.apples');
    });
    it('create fruits/apples/fuji folder', () => {
        const root = {};
        create(root, 'fruits/apples/fuji');
        expect(root).to.have.nested.property('fruits.apples.fuji');
    });
    it('create fruits/apples/fuji2 folder', () => {
        const root = {};
        create(root, 'fruits/apples/fuji2');
        expect(root).to.have.nested.property('fruits.apples.fuji2');
    });
});