import fs from 'fs';
import * as readline from "readline";
import {create} from "./commands/create";
import {list} from "./commands/list";
import {remove} from "./commands/delete";
import {move} from "./commands/move";


const fileStream = fs.createReadStream(`${__dirname}/../../command-list.txt`);
(async () => {
    const lineReader = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    const directoryTree = {};
    for await (const line of lineReader) {
        const [arg1, arg2, arg3] = line.split(' ');
        switch (arg1.trim()) {
            case 'CREATE':
                create(directoryTree, arg2.trim());
                break;
            case 'LIST':
                list(directoryTree);
                break;
            case 'MOVE':
                move(directoryTree, arg2.trim(), arg3.trim());
                break;
            case 'DELETE':
                remove(directoryTree, arg2.trim());
                break;
            default:
        }
    }
})();