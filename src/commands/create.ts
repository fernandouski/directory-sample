export const create = (repo: object, folderPath: string) => {
    const path = folderPath.split('/');
    path.reduce((accumulator, key, i) => {
        if (accumulator[key] === undefined) accumulator[key] = {};
            return accumulator[key];
        }, repo);
}