export const remove = (root: object, deleteFrom: string) => {
    const path = deleteFrom.split('/');
    const result = path.reduce((previousValue, currentKey, i) => {
        return previousValue && previousValue[currentKey];
    }, root);
    if (result) {
        removeFromNestedObject(root, path);
    } else {
        console.log(`Cannot delete ${deleteFrom} - fruit does not exist`)
    }
}

const removeFromNestedObject = (root, path) => {
    path.reduce((accumulator, key, i) => {
        if (i === path.length - 1) delete accumulator[key];
        return accumulator[key];
    }, root);
};