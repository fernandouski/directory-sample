export const move = (root: object, from: string, toDestination: string) => {
    const path = from.split('/');
    const last = path[path.length -1];
    const result = path.reduce((previousValue, currentKey, i) => {
        return previousValue && previousValue[currentKey];
    }, root);
    root[toDestination][last] = result;
    remove(root, path);
}

const remove = (root: object, path: string[]) => {
    path.reduce((accumulator, key, i) => {
        if (i === path.length - 1) delete accumulator[key];
        return accumulator[key];
    }, root);
};