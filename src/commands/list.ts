export const list = (repo: object) => {
    print(repo, 0);
}

const print = (root: object, level: number) => {
    Object.keys(root).forEach((element) => {
        let indentation = '';
        let indentationLevel = level;
        const node = root[element];
        for (let i = 0; i < indentationLevel; i++) {
            indentation += '  ';
        }
        console.log(`${indentation}${element}`);
        if (Object.keys(node).length > 0) print(node, ++indentationLevel);
        else {  print(node, 0); }
    });
}